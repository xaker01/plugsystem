<nav aria-label="Page navigation">
  <ul class="pagination">
    <?php
if(is_array($this->prev)){
    ?>
    <li>
      <a href="<?=$this->prev['offset'];?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
<?php
}
?>

<?php
if(count($this->prev_pages)){
foreach($this->prev_pages as $arr){
?>
<li><a href="<?=$arr['offset'];?>"><?=$arr['page'];?></a></li>
<?php
}
}
?>
    <li class="active"><a href="#"><?=$this->current_page?></a></li>
    <?php
    if(count($this->next_pages)){
    foreach($this->next_pages as $arr){
    ?>
    <li><a href="<?=$arr['offset'];?>"><?=$arr['page'];?></a></li>
    <?php
  }
    }
    ?>

  <?php
if(is_array($this->next)){
  ?>
    <li>
      <a href="<?=$this->next['offset'];?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
<?php
}
?>
  </ul>
</nav>
