И так это вступительная документация для обьяснения того как функционирует данная библиотека.
Для того чтобы понять,как все работает рассмотрим код ниже:


public function actionIndex()
{
    $option_frontend=array(

      'plugins_directory'=>$_SERVER['DOCUMENT_ROOT']."/frame/vendor/noistudio/plugsystem/plugins/",
      'plugins_namespace'=>'plugins',

    'theme_path'=>$_SERVER['DOCUMENT_ROOT']."/themefrontend/",
      'access'=>'',
    "query_string"=>$_SERVER['REQUEST_URI']


  );




    $construct=new FrontendManager($options);
    echo $construct->start();
    exit;
}
Итак как мы видим выше сначало идет массив с настройками.
'plugins_directory'
Сначала указывается папка где хранятся все плагины!
Внимание все плагины должны быть в рамках одной папки!
'plugins_namespace'
После указывается неймспейс в рамках которых хранятся все плагины.
'theme_path'
В данном ключе указывается путь до шаблона в котором располагается файл main.php
'access'
В данном
Если например указана 'access'=>'/secretsuperlink/',
то пока человек не зайдет по ссылке http://site.ru/secretsuperlink/
то он ничего не увидит.

"query_string"
собственно сюда нужно передавать путь по которому будет инициализирована система .
обычно это $_SERVER['REQUEST_URI']

Как это работает? ($construct=new FrontendManager($options);)
1.Сканируются все плагины из директории 'plugins_directory' ,когда находится плагин у которого
начальный адрес совпадает с первым сегментом url ,
то есть например заходя по адресу http://site.ru/first/second/third
то первым сегментом будет /first при условии что access=>'' ,
После того как плагин найден,ищется контроллер если он существует например при  queryi_string
/first/second/third
будут искаться все следущие контроллеры
в /путьдоплагина/папкаплагина/controllers/frontend/ThirdSecondFirst.php и будет вызван methodIndex
или /путьдоплагина/папкаплагина/controllers/frontend/SecondFirst.php и будет вызван methodThird
или /путьдоплагина/папкаплагина/controllers/frontend/SecondFirst.php и будет вызван methodIndex и аргумент third
или /путьдоплагина/папкаплагина/controllers/frontend/First.php и будет вызван methodSecond и аргумент third
или /путьдоплагина/папкаплагина/controllers/frontend/First.php и будет вызван methodIndex и аргументы second и third

Что далее ( $construct->start();)
Метод start в менеджере возвращает текущий html страницы,если нужно вернуть только результат из контроллера то нужно передавать в качестве аргумента true ;

Админский менеджер
Для разделения frontend и backend был разработан BackendManager работает он абсолютно по такому же принципу как FrontendManager


public function actionIndex()
{
  $option_backend=array('plugins_directory'=>$_SERVER['DOCUMENT_ROOT']."/frame/vendor/noistudio/plugsystem/plugins/",
'theme_path'=>$_SERVER['DOCUMENT_ROOT']."/themeadmin/name/",
"query_string"=>$_SERVER['REQUEST_URI'],
'admin_login'=>'admin',
'admin_password'=>'admin',
  'files_web_directory'=>'/files/tmpfiles/',
 'access'=>'/simpleadmin/'
);




    $construct=new BackendManager($options);
    echo $construct->start();
    exit;
}
Как мы видим из примера выше были добавлены следущие ключи
admin_login,admin_password  данные для входа в админку.
files_web_directory путь до файлов,куда их можно будет загружать с помощью файлового менеджера в
админке

Для того чтобы вызывать нужный менеджер в зависимости от URL есть такая штука как ConstructManager
Работает он по следующему принципу

public function actionIndex()
{
    $option_frontend=array('plugins_directory'=>$_SERVER['DOCUMENT_ROOT']."/frame/vendor/noistudio/plugsystem/plugins/",
    'theme_path'=>$_SERVER['DOCUMENT_ROOT']."/themefrontend/",
    "query_string"=>$_SERVER['REQUEST_URI'],  'access'=>'','manager'=>'\\plugsystem\\FrontendManager',

  );
    $option_backend=array('plugins_directory'=>$_SERVER['DOCUMENT_ROOT']."/frame/vendor/noistudio/plugsystem/plugins/",
  'theme_path'=>$_SERVER['DOCUMENT_ROOT']."/themeadmin/name/",
  "query_string"=>$_SERVER['REQUEST_URI'],
  'admin_login'=>'admin',
  'admin_password'=>'admin',
    'files_web_directory'=>'/files/tmpfiles/',
   'access'=>'/simpleadmin/'
  'manager'=>'\\plugsystem\\BackendManager'
);
    $options=array('frontend'=>$option_frontend,'backend'=>$option_backend);


    $construct=new ConstructManager($options);
    echo $construct->start();
    exit;
}
Как вы видите все также только в ключе manager нужно указывать полный namespace до нужного класса.


CronКоманды
Для реализации cron команд есть отдельный менеджер который называется CronManager

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use plugsystem\CronManager;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($query)
    {
        $plugins_dir=realpath(__DIR__."/../vendor/noistudio/plugsystem/plugins/");

        $option=array(
    'plugins_directory'=>$plugins_dir,
      "query_string"=>$query,  'access'=>'','manager'=>'\\plugsystem\\CronManager',

    );
        $manager=new CronManager($option);
        echo $manager->start();
    }
}

Как вы видите здесь все также и просто.
Если подытожить основную идею то она звучит так.
Единая архитектура которая полностью независима от используемых фреймворков и библиотек.
В следущих страницах документации я подробнее расскажу о  плагинах,компонентах,  и многом другом.
