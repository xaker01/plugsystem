<?php

namespace plugsystem\models;

use plugsystem\GlobalParams;

class EventModel
{
    public static $events=array();

    public static function init($obj)
    {
        $tmp_events=$obj->getEvents();
        if (count($tmp_events)) {
            foreach ($tmp_events as $event) {
                if (!isset(EventModel::$events[$event['event']])) {
                    EventModel::$events[$event['event']]=array();
                }
                EventModel::$events[$event['event']][]=array('type'=>$event['type'],'method'=>$event['method'],'class'=>$event['class']);
            }
        }
    }
    public static function isCan($event)
    {
        $isrun=GlobalParams::get($event."_is_working");
        if (is_null($isrun)) {
            GlobalParams::set($event."_is_working", true);
            return true;
        } else {
            if ($isrun===false) {
                GlobalParams::set($event."_is_working", true);
                return true;
            } else {
                return false;
            }
        }
    }
    public static function run($event, $args)
    {
        $name_event=$event;
        if (isset(EventModel::$events[$event]) and count(EventModel::$events[$event])) {
            if (EventModel::isCan($event)) {
                foreach (EventModel::$events[$event] as $event) {
                    if ($event['type']=="static") {
                        call_user_func_array($event['class']."::".$event['method'], $args);
                    } else {
                        $tmpobj=new $event['class']();
                        call_user_func_array(array($tmpobj, $event['method']), $args);
                    }
                }
                GlobalParams::set($name_event."_is_working", false);
            }
        }
    }
}
