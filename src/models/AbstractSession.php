<?php


namespace plugsystem\models;

abstract class AbstractSession
{
    abstract public function set($key, $value);
    abstract public function get($key);
    abstract public function delete($key);
    abstract public function removeAll();
}
