<?php

 namespace plugsystem\models;

class AbstractConfig
{
    protected $route;
    protected $name;
    protected $isenable;
    private $events=array();
    public function __construct($name, $route, $isenable=true)
    {
        $this->route=$route;
        $this->name=$name;
        $this->isenable=(bool)$isenable;
    }


    public function getRoute()
    {
        return $this->route;
    }
    public function isEnable()
    {
        return $this->isenable;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getEvents()
    {
        return $this->events;
    }
    protected function addEvent($namespace_path, $name_event, $method, $type="public")
    {
        if (class_exists($namespace_path) and is_string($name_event) and (isset($type) and  ($type=="public") or $type=="static")  and is_string($method)) {
            $tmp=new $namespace_path();
            if (method_exists($tmp, $method)) {
                $event_arr=array('class'=>$namespace_path,'type'=>$type,'method'=>$method,'event'=>$name_event);
                $this->events[]=$event_arr;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    private function listClass($query_string)
    {
        $list_class=array();
        $explode=explode("/", $query_string);
        $nav_to_key=array();
        if (count($explode)) {
            $max=count($explode)-1;
            foreach (array_reverse($explode) as $ekey=>$val) {
                $nav_to_key['pos_'.$ekey]=$max-$ekey;
            }
        }
        if (count($explode)) {
            $cur=count($explode)-1;
            while (isset($explode[$cur])) {
                $kk=$cur;
                $qur=$explode[$cur];
                $nextkey =$cur+1;


                if (count($list_class)) {
                    foreach ($list_class as $key=>$class) {
                        $list_class[$key]['class'].=ucfirst($qur);
                    }
                }
                $method="index";
                if (isset($explode[$nextkey])) {
                    $method=$explode[$nextkey];
                }

                $list_class[]=array('method'=>ucfirst($method),'kk'=>$kk,'class'=>ucfirst($qur),'params'=>array());
                $cur--;
            }


            foreach ($list_class as $key=>$class) {
                $list_class[$key]['class'].=ucfirst($this->name);
                $keys = array_keys($explode);
                $lastkey=end($keys);

                if ($class['kk']<$lastkey) {
                    $start=$class['kk']+1;

                    while (isset($explode[$start])) {
                        $class['params'][]=$explode[$start];
                        $start++;
                    }

                    $list_class[$key]['params']=$class['params'];
                }
            }
        }
        $method="index";
        if (isset($explode[0])) {
            $method=$explode[0];
        }

        $list_class[]=array('method'=>ucfirst($method),'class'=>ucfirst($this->name),'params'=>$explode);

        return $list_class;
    }
  private function run($obj, $method, $args)
    {
        if (method_exists($obj, $method)) {
            $classMethod = new \ReflectionMethod($obj, $method);
            $argumentCount = count($classMethod->getParameters())+1;
            if ($method!='actionIndex' or ($method=="actionIndex" and   isset($args[0]) and  strtolower($args[0])=="index")) {
                if (count($args)) {
                    array_shift($args);
                }
            }
            $i=count($args);
            if ($argumentCount and $i<$argumentCount) {
                while ($i!=$argumentCount) {
                    $args[]=0;
                    $i++;
                }
            }

            $result=call_user_func_array(array( $obj, $method), $args);
            return $result;
        } else {
            return null;
        }
    }
    private function getNamespace()
    {
        $namespace=get_class($this);
        $namespace=str_replace("\config", "", $namespace);
        return $namespace;
    }
    public function findResult($query_string, $option)
    {
        $result=null;
        $array_of_class=$this->listClass($query_string);
        $namespace=$this->getNamespace();


        if (count($array_of_class)) {
            foreach ($array_of_class as $arr_class) {
                $class = '\\'.$namespace.'\\controllers\\'.$option['folder']."\\".$arr_class['class'];

                if (class_exists($class)) {
                    $obj=new $class;

                    if (method_exists($obj, 'action'.$arr_class['method'])) {
                        $result=$this->run($obj, 'action'.$arr_class['method'], $arr_class['params']);
                    } else {
                        $result=$this->run($obj, 'actionIndex', $arr_class['params'], $arr_class['params']);
                    }
                    break;
                }
            }
        }
        return $result;
    }
}
