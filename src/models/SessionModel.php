<?php

 namespace plugsystem\models;

use Yii;

class SessionModel extends AbstractSession
{
    public function set($key, $value)
    {
        $session = Yii::$app->session;
        $session[$key] =$value;
    }
    public function get($key)
    {
        $session = Yii::$app->session;
        if ($session->has($key)) {
            return $session[$key];
        } else {
            return null;
        }
    }
    public function removeAll()
    {
        $session=Yii::$app->session();
        $session->removeAll();
    }
    public function delete($key)
    {
        $session = Yii::$app->session;
        if ($session->has($key)) {
            $session->remove($key);
            return true ;
        } else {
            return false;
        }
    }
}
