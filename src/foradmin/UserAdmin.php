<?php

namespace plugsystem\foradmin;

use plugsystem\GlobalParams;
use plugsystem\models\NotifyModel;

class UserAdmin
{
    public static function isLogin()
    {
        $session=GlobalParams::$session;
        return (bool)$session->get("admin_is_login");
    }
    public static function check()
    {
        $option=GlobalParams::get("main_setting");
        $result=false;
        if (isset($option['admin_login']) and isset($option['admin_login'])) {
            if (isset($_POST['login']) and $_POST['login']==$option['admin_login'] and isset($_POST['password']) and $_POST['password']==$option['admin_password']) {
                $session=GlobalParams::$session;
                $session->set("admin_is_login", true);
                $result=true;
            }
        } else {
        }
        $helper=new Helper();
        if (!$result) {
            NotifyModel::add("Вы ввели неправильный логин или пароль!");
            $helper->redirect("/index");
        } else {
            $helper->redirect("/index");
        }
    }
}
