<?php

namespace plugsystem\foradmin;

use plugsystem\GlobalParams;
use Yii;

class Helper
{
    private $option=array();
    private $objs=array();
    public function __construct()
    {
        $this->option=GlobalParams::get("main_setting");
    }
    public function link($href)
    {
        return $this->option['access'].$href;
    }
    public function redirect($link, $inner=true)
    {
        $url=$this->option['access'].$link;
        if (!$inner) {
            $url=$link;
        }
        Yii::$app->response->redirect($url, 301);
      //  header("Location: ".$url);
        Yii::$app->end();
    }
    public function returnback($url="/")
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $this->redirect($_SERVER['HTTP_REFERER'], false) ;
        } else {
            $this->redirect("/") ;
        }
    }

    public function c($name)
    {
        $class=$this->option['namespace_components'].''.ucfirst($name);
        if (class_exists($class)) {
            if (!isset($this->objs[$name])) {
                $this->objs[$name]=new $class;
            }
            return $this->objs[$name];
        }
    }
}
