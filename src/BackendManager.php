<?php

namespace plugsystem;

use plugsystem;

use plugsystem\foradmin\UserAdmin;
use plugsystem\models\PluginModel;
use plugsystem\models\ViewModel;

class BackendManager extends \plugsystem\core\AbstractManager
{
    protected $option=array(

  'path_to_file'=>false,
  'basic_route'=>'/',
  'access'=>'/pathtoadmin/',
  'plugins_namespace'=>'plugins',
  'plugins_directory'=>'',
  'theme_path'=>'',
  'query_string'=>'',
  'session_class'=>'\\plugsystem\models\\SessionModel',
  'namespace_components'=>'\\plugcomponents\\',
  'admin_login'=>'admin',
  'admin_password'=>'admin',
  'default_paginator_file'=>__DIR__.'/../paginator.php',
  'files_web_directory'=>'/files/tmpfiles/'


  );

    public function __construct($option_array=array())
    {
        $option_array['type']="backend";
        if (isset($_SERVER['DOCUMENT_ROOT']) and !isset($option['files_path_directory'])) {
            $option_array['files_path_directory']=$_SERVER['DOCUMENT_ROOT']."/files/tmpfiles/";
        }




        parent::__construct($option_array);
        if ($this->option['query_string']=="/doauth") {
            UserAdmin::check();
        }

        if (!UserAdmin::isLogin()) {
            if ($this->option['query_string']=="/index" or $this->option['query_string']=="/") {
            } else {
                GlobalParams::$helper->redirect("index");
            }
        }
    }

    protected function render($result)
    {
        $file="login.php";
        if (UserAdmin::isLogin()) {
            $file="main.php";
        }
        $data=array();
        $data['plugin']=$result;

        $view=new ViewModel($this->getViewPath($file), $data);
        return  $view->render();
    }
}
