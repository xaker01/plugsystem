<?php
namespace plugsystem;

use plugsystem\models\EventModel;
use plugsystem\models\PluginModel;
use plugsystem\models\ViewModel;

class ConstructManager
{
    private $class=null;
    public function __construct($options)
    {
        $found=null;

        if (count($options)) {
            foreach ($options as $option) {
                if ($option['access']=="") {
                } else {
                    $explode=explode($option['access'], $option['query_string']);

                    if ($explode[0]=="") {
                        if (!isset($explode[1])) {
                            $explode[1]="";
                        }
                        $option['query_string']="/".$explode[1];
                        $found=$option;
                        break;
                    }
                }
            }
            if (is_null($found)) {
                foreach ($options as $option) {
                    if ($option['access']=="") {
                        $found=$option;
                        break;
                    }
                }
            }
        }

        if (is_array($option)) {
            $this->class=new $option['manager']($option);
        }
    }
    public function start()
    {
        $result=$this->class->start();
      
        return $result;
    }
}
